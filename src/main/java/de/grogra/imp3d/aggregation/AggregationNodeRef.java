package de.grogra.imp3d.aggregation;

import de.grogra.graph.ChangeBoundaryListener;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.GraphUtils;
import de.grogra.graph.Path;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp.View;
import de.grogra.imp.edit.ViewSelection;
import de.grogra.imp3d.View3D;
import de.grogra.persistence.Shareable;
import de.grogra.persistence.SharedObjectProvider;
import de.grogra.persistence.SharedObjectReference;
import de.grogra.persistence.Transaction;
import de.grogra.pf.registry.NodeReference;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Showable;
import de.grogra.pf.ui.Window;

public class AggregationNodeRef extends NodeReference 
	implements Showable, SharedObjectReference, SharedObjectProvider,
	ChangeBoundaryListener {
		//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new AggregationNodeRef ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new AggregationNodeRef ();
	}

//enh:end
	
	private AggregationNodeRef() {
		this(null);
	}
	
	public AggregationNodeRef(Node ref) {
		super(ref);
	}
	
	@Override
	public final Object getObject ()
	{
		return getSharedObject ();
	}


	public final Shareable getSharedObject ()
	{
		return (Shareable) getBaseObjectImpl ();
	}

	@Override
	protected void activateImpl ()
	{
		super.activateImpl ();
		getSharedObject().initProvider(this);
		getSharedObject().addReference(this);
		Graph g = ((Node)getSharedObject()).getGraph();
		g.addChangeBoundaryListener(this);
	}

	@Override
	protected void deactivateImpl ()
	{
		Node n =(Node)getSharedObject();
		n.removeReference(this);
		n.getGraph().removeChangeBoundaryListener(this);
		if (!((n.getGraph().getLifeCycleState(n, true) & (Graph.TRANSIENT | Graph.PERSISTENT_DELETED))!=0)) {
			n.removeAll (n.getGraph ().getActiveTransaction ());
		}
	}
	
	//interacts with the node throught the explorer 
	//selects all the children
	@Override
	public void show(Context ctx) {
		// check if one of the two view (2D and 3D) is open (with 3D in priority). 
		View view = View3D.getDefaultView(ctx);
		if (view == null) {
			Window w=ctx.getWindow();
			view = (w != null) ? (View) w.getPanel ("/ui/panels/2d/graph") : null;
			if (view==null) {
				return;
			}
		}
		ViewSelection s = ViewSelection.get (view);
		s.set (ViewSelection.SELECTED, Path.PATH_0, true);
		AggregationNode node = (AggregationNode) getSharedObject ();

		for(Edge e = node.getFirstEdge(); e != null; e = e.getNext(node))
		{
			if(e.testEdgeBits(Graph.REFINEMENT_EDGE) && e.isSource(node))
			{
				Node target = e.getTarget();
				Path p = GraphUtils.getTreePath(view.getWorkbenchGraphState(), target, true);
				s.toggle(ViewSelection.SELECTED, p);
			}
		}		
	}

	/**
	 * This listener follow the change in attributes of the AggNode 
	 */
	@Override
	public void sharedObjectModified(Shareable object, Transaction t) {
		if (object == getSharedObject()) {
			// change name?
		}
	}

	/**
	 * This listeners follow changes in the graph. If at  the end of a change the AggNode ref is 
	 * deleted or transient. The ref is removed.
	 */
	@Override
	public void beginChange(GraphState gs) {
	}
	@Override
	public void endChange(GraphState gs) {
		Node n = ((Node)getSharedObject());
		if ((n.getGraph().getLifeCycleState(n, true) & (Graph.TRANSIENT | Graph.PERSISTENT_DELETED))!=0) {
			deactivate();
			remove();
		}
	}
	@Override
	public int getPriority() {
		return ChangeBoundaryListener.TOPOLOGY_PRIORITY;
	}
}
