package de.grogra.imp3d.aggregation;

import de.grogra.xl.impl.base.*;

import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.selection.Commands;
import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.objects.SceneBoundaries;
//import de.grogra.imp3d.selection.Transaction;
import de.grogra.pf.ui.Context;
import de.grogra.rgg.RGGRoot;

/**
 * function collection for helpful methods for various functions 
 */
public class GraphAggregationTools {
	
	private Null edge = new Null();
	
	/**
	 * searches for the minimal and maximal node locations of the tree for every axis
	 * to create a bounding area for the tree in the scene
	 */
	protected static SceneBoundaries getSceneBoundaries(Context ctx) {
		SceneBoundaries boundaries = new SceneBoundaries();
		Graph graph = ctx.getWorkbench().getRegistry().getProjectGraph();
		Node root = (Node) graph.getRoot(Graph.MAIN_GRAPH);
		boundaries.init(root.getCurrentGraphState(), EdgePatternImpl.TREE);
		graph.accept(root, boundaries, null);
		return boundaries;
	}
	
	//excludes the Node 0 out of the aggregation
	protected static void removeZeroRGG (List<Node> nodes) {
		List<Node> toRemove = new ArrayList<>();
		
		for(Node node : nodes) {
			if((node.getId() == 0) || (node instanceof RGGRoot)) {
				toRemove.add(node);
			}
		}
		nodes.removeAll(toRemove);
	}
}

	