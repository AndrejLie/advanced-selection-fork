package de.grogra.imp3d.aggregation;

import de.grogra.graph.GraphState;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.edit.GraphSelectionImpl;
import de.grogra.pf.ui.edit.Selectable;
import de.grogra.pf.ui.edit.Selection;

/**
 * An aggregation Node is a simple node. It represents a aggregation or a grouping of nodes
 * that were previously selected or fulfil a spatial coherence. Aggregation nodes 
 * usually do not have parent nodes expect other aggregation nodes.  
 * Its children are part of an aggregation. They are linked by refinement edges,
 * where the aggregation node is the source and the children the target of the connection.
 */
public class AggregationNode extends de.grogra.graph.impl.Node implements Selectable {
	
	public AggregationNode () {
		super(); 
	}

	/**
	 * Selectable is Required to skip the attribute editor of the reference
	 */
	@Override
	public Selection toSelection(Context ctx) {
		return new GraphSelectionImpl (ctx, GraphState.get (ctx.getWorkbench ()
				.getRegistry ().getProjectGraph (), UI.getThreadContext (ctx)),
				this, true);
	}

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new AggregationNode ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new AggregationNode ();
	}

//enh:end
}
