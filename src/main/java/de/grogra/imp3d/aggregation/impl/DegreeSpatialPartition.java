package de.grogra.imp3d.aggregation.impl;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3d;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.aggregation.AggregationNode;
import de.grogra.imp3d.aggregation.SpatialPartition;
import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.SelectionTools;
import de.grogra.imp3d.selection.impl.AngleSelection;
import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.rgg.Library;

public class DegreeSpatialPartition extends SpatialPartition{

	Interval angle;
	int nPartitions;
	
	public DegreeSpatialPartition () {
		angle = null;
	}
	
	public DegreeSpatialPartition (int nPartitions, int axisMask) {
		this.nPartitions = nPartitions;
		this.angle = new Interval (0.0, 0.0, axisMask);
		this.addSpatialPartition(this);
	}
	
	public DegreeSpatialPartition (double startAngle, double endAngle, int axisMask) {
		this.angle = new Interval (startAngle, endAngle, axisMask);
	}
	
	public DegreeSpatialPartition (Interval angle) {
		this.angle = angle;	
	}
	
	public Interval getAngle () {
		return this.angle;
	}

	/**
	 * creates a n partitions around a given axis with the degree span of 360°/ n
	 */
	@Override
	protected void createAllSpatialPartition () {
		double degreeSpan = 360/this.nPartitions;
		for(int i = 0; i < nPartitions; i++) {
			double startAngle = degreeSpan*i;
			double endAngle = degreeSpan*(i+1) - epsilon;
			DegreeSpatialPartition dsp = new DegreeSpatialPartition(startAngle, endAngle, this.getAngle().getAxisMask());
			this.addSpatialPartition(dsp);
		}
	}
	
	//TODO this can be excluded from the code
	@Override
	protected void selection () {
		for(SpatialPartition sp : this.partitionList) {
			Interval a = ((DegreeSpatialPartition) sp).getAngle();
			selection = new AngleSelection(a, false);
			sp.setSelection(selection);
		}
		DegreeSpatialPartition spZero = (DegreeSpatialPartition) partitionList.get(0);
		spZero.setSelection(new AngleSelection(spZero.getAngle(), true)); 
	}
	
	public Interval getInterval() {
		return this.angle;
	}
}
