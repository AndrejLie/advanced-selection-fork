package de.grogra.imp3d.aggregation.impl;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3d;

import de.grogra.imp3d.aggregation.SpatialPartition;
import de.grogra.imp3d.selection.impl.AngleSelection;
import de.grogra.imp3d.selection.impl.AxisIntervalSelection;
import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.imp3d.selection.objects.SceneBoundaries;
import de.grogra.rgg.Library;
import de.grogra.graph.impl.Node;

public class PlaneSpatialPartition extends SpatialPartition{
	
	protected double size;
	protected double translation = 0.0;
	protected SceneBoundaries boundaries;
	protected Interval interval = new Interval();
	protected List<Node> selected = new ArrayList<>();
	
	public PlaneSpatialPartition () {
	}
	
	public PlaneSpatialPartition (double size, int axisMask) {
		this.interval.setAxisMask(axisMask);
		this.size = size;
	}
	
	public PlaneSpatialPartition (double size, double translation, int axisMask) {
		this(size, axisMask);
		this.translation = translation;
	}
	
	public PlaneSpatialPartition (SceneBoundaries boundaries, double size, double translation, int axisMask) {
		this(size, translation, axisMask);
		this.boundaries = boundaries;
	}
	
	public PlaneSpatialPartition (Interval interval) {
		this.interval = interval;
	}
	
	@Override
	protected void createAllSpatialPartition() {
		
		if (this.interval.getAxisMask() == 1) {
			for(double distance = translation; distance <= boundaries.getMaxX(); distance += size) {
				createPlaneSpatialPartition(distance, size, this.interval.getAxisMask());
			}
			for(double distance = translation - size; distance > boundaries.getMinX() - size; distance -= size) {
				createPlaneSpatialPartition(distance, size, this.interval.getAxisMask());
			}
		}
		if (this.interval.getAxisMask() == 2) {
			for(double distance = translation; distance <= boundaries.getMaxY(); distance += size) {
				createPlaneSpatialPartition(distance, size, this.interval.getAxisMask());
			}
			for(double distance = translation - size; distance > boundaries.getMinY() - size; distance -= size) {
				createPlaneSpatialPartition(distance, size, this.interval.getAxisMask());
			}
		}
		if (this.interval.getAxisMask() == 4) {
			for(double distance = translation; distance <= boundaries.getMaxZ(); distance += size) {
				createPlaneSpatialPartition(distance, size, this.interval.getAxisMask());
			}
			for(double distance = translation - size; distance > boundaries.getMinZ() - size; distance -= size) {
				createPlaneSpatialPartition(distance, size, this.interval.getAxisMask());
			}
		}
	}
	
	
	private void createPlaneSpatialPartition (double translation, double size, int axisMask) {
		double start = translation;
		double end = translation + size;
		Interval intvl = new Interval (start, end, this.interval.getAxisMask());
		PlaneSpatialPartition partition = new PlaneSpatialPartition (intvl);
		this.addSpatialPartition(partition);
		
	}
	
	//TODO not needed
	@Override
	protected void selection() {
		for(SpatialPartition sp : this.partitionList) {
			Interval a = ((PlaneSpatialPartition) sp).getInterval();
			selection = new AxisIntervalSelection(a);
			sp.setSelection(selection);
		}	
	}
	
	public void setBoundaries (SceneBoundaries boundaries) {
		this.boundaries = boundaries;
	}
	
	protected void setInterval(double start, double end) {
		this.interval.setStart(start);
		this.interval.setEnd(end);
	}
	
	public Interval getInterval() {
		return interval;
	}	
}
