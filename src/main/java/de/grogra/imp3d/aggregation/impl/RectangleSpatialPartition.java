package de.grogra.imp3d.aggregation.impl;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.aggregation.SpatialPartition;
import de.grogra.imp3d.selection.impl.AxisIntervalSelection;
import de.grogra.imp3d.selection.impl.RectangleIntervalSelection;
import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.imp3d.selection.objects.SceneBoundaries;
import de.grogra.rgg.Library;

public class RectangleSpatialPartition extends SpatialPartition{
	
	protected Interval x,y,z;
	//interval/rectangle size
	protected double size;
	//bounding area of graph
	protected SceneBoundaries boundaries;
	
	List<Interval> IntervalsX = new ArrayList<>();
	List<Interval> IntervalsY = new ArrayList<>();
	List<Interval> IntervalsZ = new ArrayList<>();
	
	public RectangleSpatialPartition () {
	}
	
	public RectangleSpatialPartition (SceneBoundaries boundaries,double size) {
		this.size = size;
		this.boundaries = boundaries;
	}
	
	public RectangleSpatialPartition (Interval x, Interval y, Interval z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	/**
	 * devides the spatial graph in the scene into rectangles of the same size
	 * and creates a partition for every spatial division - cutting the true into
	 * cubes.
	 */
	@Override
	protected void createAllSpatialPartition() {
		for(double width = this.boundaries.getMinX(); width <= this.boundaries.getMaxX(); width += this.size) {			
			for(double length = this.boundaries.getMinY(); length <= this.boundaries.getMaxY(); length += this.size) {
				for(double height = this.boundaries.getMinZ(); height <= this.boundaries.getMaxZ(); height += this.size) {
					//a cubic partition is represented as three intervals - one for every axis
					Interval intvlX = new Interval(width, width + this.size, Interval.X_AXIS);
					Interval intvlY = new Interval(length, length + this.size, Interval.Y_AXIS);
					Interval intvlZ = new Interval(height, height + this.size, Interval.Z_AXIS);
					RectangleSpatialPartition rsp = new RectangleSpatialPartition (intvlX, intvlY, intvlZ);
					this.partitionList.add(rsp);
				}
			}
		}
	}

	//TODO not needed
	@Override
	protected void selection() {
		for(SpatialPartition sp : this.partitionList) {
			Interval a = ((RectangleSpatialPartition) sp).x;
			Interval b = ((RectangleSpatialPartition) sp).y;
			Interval c = ((RectangleSpatialPartition) sp).z;
			selection = new RectangleIntervalSelection(a,b,c);
			sp.setSelection(selection);
		}	
	}
	
	public Interval getIntervalX () {
		return this.x;
	}
	
	public Interval getIntervalY () {
		return this.y;
	}
	
	public Interval getIntervalZ () {
		return this.z;
	}
}
