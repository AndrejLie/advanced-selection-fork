package de.grogra.imp3d.aggregation;

import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.Graph;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.impl.ParentNTypeSelection;
import de.grogra.util.I18NBundle;
/**
 * Abstract super class defines fundamental fields and methods for every following partition. 
 * It provides a polymorphoristic data type for storage. Spatial Partition separate the scene
 * into multiple partition based on user given menu function and input parameters.
 * For every partition is a selection created, that assigns nodes based on the type of spatial 
 * partition and given parameters 
 */
public abstract class SpatialPartition extends VisitorImpl{ 
	
	protected Selection selection= null;
	protected AggregationNode aggNode = null;
	public static final I18NBundle I18N = I18NBundle.getInstance(SpatialPartition.class);
	//range for assigning nodes to intervals epsilon = 1e-10	 
	protected final static double epsilon = 0.000000001;
	protected static List<SpatialPartition> partitionList = new ArrayList<>();

	protected abstract void createAllSpatialPartition();
	
	public void addSpatialPartition(SpatialPartition sp) {
		if (partitionList.contains(sp)) {
			return;
		}
		partitionList.add(sp);
	}
	
	//TODO: check if getting aggregation nodes
	public List<Node> getAggregatedNodes(Object obj) {
		Selection sel = new ParentNTypeSelection(AggregationNode.$TYPE);
		return sel.getSelectedNodes();
	}
	
	//TODO i dont think this is needed
	protected abstract void selection ();
    
	public void setSelection (Selection sel) {
		this.selection = sel;
	}
	
	public Selection getSelection() {
		return selection;
	}

	public AggregationNode getAggregation() {
		return this.aggNode;
	}
	
	public void setAggregation(AggregationNode aggNode) {
		this.aggNode = aggNode;
	}
	
	public List<SpatialPartition> getPartitionList () {
		return this.partitionList;
	}
	
	protected void aggregate () {
		for(SpatialPartition sp : partitionList) {
			List<Node> aggregationChildren = sp.getSelection().getSelectedNodes();
			if(!aggregationChildren.isEmpty()) {
				sp.aggNode = new AggregationNode();
				for(Node node : aggregationChildren) {
					sp.getAggregation().addEdgeBitsTo(node, Graph.REFINEMENT_EDGE, null);
				}
			}
		}
	}
	
	public void dispose () {
		selection = null;
		aggNode = null;
		partitionList.clear();
	}
	
}
