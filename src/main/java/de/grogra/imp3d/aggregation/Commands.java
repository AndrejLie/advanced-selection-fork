package de.grogra.imp3d.aggregation;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3d;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.imp3d.aggregation.impl.DegreeSpatialPartition;
import de.grogra.imp3d.aggregation.impl.PlaneSpatialPartition;
import de.grogra.imp3d.aggregation.impl.RectangleSpatialPartition;
import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.SelectionTools;
import de.grogra.imp3d.selection.dialogs.InputDialogDegreeSelection;
import de.grogra.imp3d.selection.dialogs.InputDialogDegreeSpatialPartition;
import de.grogra.imp3d.selection.dialogs.InputDialogIntervalSpatialPartition;
import de.grogra.imp3d.selection.dialogs.InputDialogRectangleSpatialPartition;
import de.grogra.imp3d.selection.impl.AllNodesSelection;
import de.grogra.imp3d.selection.impl.AngleSelection;
import de.grogra.imp3d.selection.impl.AxisIntervalSelection;
import de.grogra.imp3d.selection.impl.NTypeSelection;
import de.grogra.imp3d.selection.impl.ParentNTypeSelection;
import de.grogra.imp3d.selection.impl.PlaneSelection;
import de.grogra.imp3d.selection.impl.RectangleIntervalSelection;
import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.imp3d.selection.objects.SceneBoundaries;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Context;
import de.grogra.rgg.Library;
import de.grogra.rgg.RGGRoot;

/**
 * Direct commands in the GUI that execute the functionality for the category of aggregation and partitioning.
 * Every new node added to the graph has to be registered and disposed. the dispose is necessary to
 * clear existing partitions of previous models. 
 * Required parameters are given by an additional input panel.
 */

public class Commands {
	//TODO ask Gaetan about this, if something needs to be added
	//ensure the node is added to the explorer panel
	private static void registerAggregationNode(AggregationNode agg, Context ctx) {
		Registry r = ctx.getWorkbench().getRegistry();
		AggregationNodeRef ref = new AggregationNodeRef (agg);
		Item dir = r.getDirectory("/project/objects/aggregations", null);
		dir.addUserItemWithUniqueName(ref, "AggNode: "+Long.toString( agg.getId()));
	}

	/** 
	 * Creates an aggregation node and connects every selected node to it.
	 * The aggregation is the source and the selected nodes are the target of 
	 * refinement edges that are used for the connection
	 */
	public static void aggregateSelected (Item item, Object info, Context ctx)
	{
		List<Node> nodes = SelectionTools.getSelectedNodes(ctx);
		if (nodes==null) {
			return;
		}
		AggregationNode agg = new AggregationNode();
		for (Node n : nodes) {
			//link every node that was selected to the aggregation with refinement edges
			agg.addEdgeBitsTo (n, de.grogra.graph.Graph.REFINEMENT_EDGE, null);
		}
		if (!nodes.isEmpty()) {
			registerAggregationNode(agg,ctx);
		}
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("aggregate_selected_done"));
	}
	
	/**
	 * Creates a aggregation node and connects every aggregation node without 
	 * a parent to it. The new node is the source and the connections are
	 * edges of the type refinement 
	 */
	public static void aggregateAggregations (Item item, Object info, Context ctx)
	{
		//get every node of the type 'AggregationNode'
		Selection sel = new NTypeSelection("de.grogra.imp3d.aggregation.AggregationNode");
		SelectionTools.innitializeSelection(sel, ctx);
		List <Node> nodes = sel.getSelectedNodes();
		
		AggregationNode agg = new AggregationNode();
		for (Node n : nodes) {
			//only aggregations without a parent aggregation
			if(n.getSource() == null) {
				//link every node of the type aggregation and without a parent to a new aggregation
				agg.addEdgeBitsTo (n, de.grogra.graph.Graph.REFINEMENT_EDGE, null);
			}
		}
		if (!sel.getSelectedNodes().isEmpty()) {
			registerAggregationNode(agg,ctx);
		}
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("aggregation_of_aggregations_done"));
	}
	
	/**
	 * Divides the space of the tree around an axis, selects all nodes for each spatial partition
	 * and aggregates all set of nodes to the corresponding partition.
	 */
	public static void aggregateDegreeSpatialPartition(Item item, Object info ,Context ctx) 
	{	
		InputDialogDegreeSpatialPartition inputPanel = new InputDialogDegreeSpatialPartition();
		if (!inputPanel.getInput()) {
			System.err.println("Input Panel for angular Spatial Partition has failed");
			return;
		}
		int nIntervals = inputPanel.getNSectors();
		int axisMask = inputPanel.getAxisMask();
		
		SpatialPartition partition = new DegreeSpatialPartition(nIntervals, axisMask);
		partition.createAllSpatialPartition();
		
		//TODO change this 
		partition.selection();
		
		//every partition has its own selection that is initiated , which stores the spatial designated nodes
		for(SpatialPartition sp : partition.getPartitionList()) {
			//System.err.println("Interval start:"+((DegreeSpatialPartition)sp).getInterval().getStart()+" , "+"end:"+((DegreeSpatialPartition)sp).getInterval().getEnd());
			Selection sel = sp.getSelection();
			SelectionTools.select(SelectionTools.innitializeSelection((sel), ctx), ctx);
			//excludes Node 0 out of the aggregation
			GraphAggregationTools.removeZeroRGG(sel.getSelectedNodes());
			
		}
//		System.err.println("--- Special Interval [0,0]---");
//		for( Node node : partition.getPartitionList().get(0).getSelection().getSelectedNodes()){
//			Point3d p = Library.location(node);
//			System.err.println("NodeID: "+node.getId()+" Position: ("+p.x+", "+p.y+", "+p.z+")");
//		}
		partition.aggregate();
		register(partition, ctx);
		partition.dispose();
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("spatial_partition_degree_done"));
	}
	
	/**
	 * Divides the space of the tree into spatial layers on an axis, 
	 * selects all nodes for each spatial partition and aggregates 
	 * all set of nodes to the corresponding partition.
	 */
	public static void aggregatePlaneSpatialPartition(Item i, Object info, Context ctx) {
		InputDialogIntervalSpatialPartition inputPanel = new InputDialogIntervalSpatialPartition();
		if (!inputPanel.getInput()) {
			System.err.println("Input Panel for planar Spatial Partition has failed");
			return;
		}
		SceneBoundaries boundaries = GraphAggregationTools.getSceneBoundaries(ctx);
		double translation = inputPanel.getTranslation();
		double size = inputPanel.getRange();
		int axisMask = inputPanel.getAxisMask();
		
		SpatialPartition partition = new PlaneSpatialPartition(boundaries, size, translation, axisMask);		
		partition.createAllSpatialPartition();
		
		//every partition has its own selection that is initiated , which stores the spatial designated nodes
		for(SpatialPartition sp : partition.getPartitionList()) {
			sp.setSelection( new AxisIntervalSelection( ((PlaneSpatialPartition)sp).getInterval() ));
			
			System.err.println("Interval: ["+((PlaneSpatialPartition)sp).getInterval().getStart()+", "+
											((PlaneSpatialPartition)sp).getInterval().getEnd()+"]");
			
			Selection sel = sp.getSelection();
			SelectionTools.select(SelectionTools.innitializeSelection(sel, ctx), ctx);
			//excludes Node 0 out of the aggregation
			GraphAggregationTools.removeZeroRGG(sel.getSelectedNodes());
		}
		partition.aggregate();
		register(partition, ctx);
		partition.dispose();
		//partition.printOut();
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("spatial_partition_plane_done"));
	}
	
	/**
	 * Divides the space of the tree into spatial cubes, selects all nodes for each spatial partition
	 * and aggregates all set of nodes to the corresponding partition.
	 */
	public static void aggregateRectangleSpatialPartition (Item i, Object info, Context ctx) {
		InputDialogRectangleSpatialPartition inputPanel = new InputDialogRectangleSpatialPartition();
		if (!inputPanel.getInput()) {
			System.err.println("Input Panel for rectengular Spatial Partition has failed");
			return;
		}
		
		SceneBoundaries boundaries = GraphAggregationTools.getSceneBoundaries(ctx);
		double size = inputPanel.getSize();
		SpatialPartition partition = new RectangleSpatialPartition(boundaries, size);		
		partition.createAllSpatialPartition();
		
		//every partition has its own selection that is initiated , which stores the spatial designated nodes
		for(SpatialPartition sp : partition.getPartitionList()) {
			Interval x = ((RectangleSpatialPartition)sp).getIntervalX();
			Interval y = ((RectangleSpatialPartition)sp).getIntervalY();
			Interval z = ((RectangleSpatialPartition)sp).getIntervalZ();
			sp.setSelection( new RectangleIntervalSelection(x,y,z));
			Selection sel = sp.getSelection();
			SelectionTools.select(SelectionTools.innitializeSelection(sel, ctx), ctx);
			//excludes Node 0 out of the aggregation
			GraphAggregationTools.removeZeroRGG(sel.getSelectedNodes());
		}
		partition.aggregate();
		register(partition, ctx);
		partition.dispose();
		//partition.printOut();
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("spatial_partition_plane_done"));
	}
	
	//TODO ask Gaetan about this
	public static void register(SpatialPartition partition, Context ctx) {
		for(SpatialPartition sp : partition.getPartitionList()) {
			if(!(sp.getAggregation() == null)) {
				registerAggregationNode(sp.getAggregation(), ctx);
			}
		}
	}
}
