package de.grogra.imp3d.selection;

import java.util.List;

import javax.vecmath.Point3d;

import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.imp3d.aggregation.AggregationNode;
import de.grogra.imp3d.selection.dialogs.DialogTool;
import de.grogra.imp3d.selection.dialogs.InputDialogDegreeSelection;
import de.grogra.imp3d.selection.dialogs.InputDialogNType;
import de.grogra.imp3d.selection.impl.AllNodesSelection;
import de.grogra.imp3d.selection.impl.AncestorSelection;
import de.grogra.imp3d.selection.impl.AngleSelection;
import de.grogra.imp3d.selection.impl.AxisIntervalSelection;
import de.grogra.imp3d.selection.impl.NTypeSelection;
import de.grogra.imp3d.selection.impl.ParentNTypeSelection;
import de.grogra.imp3d.selection.impl.VolumeSelection;
import de.grogra.imp3d.selection.impl.selectedNodesSelection;
import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.imp3d.selection.impl.PlaneSelection;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.rgg.Library;

/**
 * List of GroIMP commands that call all selections.
 * The commands are defined to be used from the GUI as menu items.
 */
public class Commands {
			
	/**
	 * Select all children of the given ancestor.
	 * 
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void selectByAncestor (Item item, Object info, Context context)
	{
		Node n = SelectionTools.getSelectedNode(context);
		Selection sel = new AncestorSelection(n);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, context), context);
		context.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_ancestor_done"));
	}
		
	/**
	 * To use this function you have to insert a primitive volume object into the scene and have it selected after being
	 * rotated, translated or transformed in the the coordinate system. Then you can use it via the menu entry.
	 * The Function then selects every visible node in the primitive volume.
	 * 
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void selectByPrimitiveVolume(Item item, Object info, Context ctx)
	{	
		Node n = SelectionTools.getSelectedNode(ctx);
		
		if(n == null) {
			boolean exeption = DialogTool.handleException("errorPrimitive", "labelErrorPrimitive");
			return;
		}
		Selection sel = new VolumeSelection(n);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, ctx), ctx);
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_volumetric_object_done"));
	}

	
	/**
	 * initialization method of the menu entry 
	 * 
	 * To use this function you have to insert a plane into the scene and have it selected after being
	 * rotated, translated or transformed in the the coordinate system. Then you can use it via the menu entry.
	 * The Function then selects every visible node over the selected plane.
	 * 
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void selectByPlaneOver(Item item, Object info, Context ctx)
	{
		Node n = SelectionTools.getSelectedNode(ctx);
		Selection sel = new PlaneSelection(n, true);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, ctx), ctx);
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_plane_over_done"));
	}
	
	
	/**
	 * To use this function you have to insert a plane into the scene and have it selected after being
	 * rotated, translated or transformed in the the coordinate system. Then you can use it via the menu entry.
	 * The Function then selects every visible node under the selected plane.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void selectByPlaneUnder(Item item, Object info, Context ctx)
	{
		Node n = SelectionTools.getSelectedNode(ctx);
		Selection sel = new PlaneSelection(n, false);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, ctx), ctx);
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_plane_under_done"));
	}

	/**
	 * Select all nodes that are part of an aggregation
	 */
	public static void selectAggregationChildren (Item item, Object info, Context ctx)
	{
		Selection sel = new ParentNTypeSelection(AggregationNode.$TYPE);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, ctx), ctx);
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_children_of_aggregation_done"));		
	}
	
	public static void selectByAxisInterval (Item item, Object info, Context ctx) {
		InputDialogAxisIntervalSelection inputPanel = new InputDialogAxisIntervalSelection();
		
		if(!inputPanel.getInput()) {
			System.err.println("Input Panel for axial Interval Selection has failed");
			return;
		}
		int axisMask = inputPanel.getAxisMask();
		double start = inputPanel.getStart();
		double end = inputPanel.getEnd();
		
		Interval intvl = new Interval (start, end, axisMask);
		
		System.err.println("Interval: ["+intvl.getStart()+", "+intvl.getEnd()+"]"+" axisMask: "+intvl.getAxisMask());
		
		Selection sel = new AxisIntervalSelection(intvl);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, ctx), ctx);
		
//		System.err.println("Interval: ["+start+", "+end+"]");
//		for(Node node: sel.getSelectedNodes()) {
//			Point3d point = Library.location(node);
//			System.err.println("NodeId: "+node.getId()+" Position: ["+point.x+", "+point.y+", "+point.z+"]");
//		}
		
		ctx.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_degree_done"));
	}

	/**
	 * This function selects nodes based on a span of degrees for as many graphs as selected 
	 * by at least a single node in in the corresponding graph. To use this function 
	 * you have to press the menu button in the viewer menu under Selection -> 'polarSelect'
	 * and fill out the pop up by setting start and end angle and an option to optionally
	 * include the stem, which represents all nodes that have no distance to the root as
	 * the root itself because it has a mathematical distance of zero.
	 * */
	
	public static void selectByDegreeInterval(Item item, Object info ,Context context)
	{	
		InputDialogDegreeSelection inputPanel = new InputDialogDegreeSelection();
		if (!inputPanel.getInput()) {
			System.err.println("Input Panel for Degree Selection has failed");
			return;
		}	
		double startAngle = inputPanel.getStartAngle();
		double endAngle = inputPanel.getEndAngle();
		boolean includeZeroRadiusNode = inputPanel.isZeroRadiusNodeIncluded();
		int axisMask = inputPanel.getAxisMask();
		Interval angle = new Interval(startAngle, endAngle, axisMask);
		
		//System.err.println("DegreeInterval start:"+startAngle+" end:"+endAngle+"/n");
		
		Selection sel = new AngleSelection(angle, includeZeroRadiusNode);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, context), context);
		
		context.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_degree_done"));
	}
	
	public static void deleteSelectedNodes (Item item, Object info, Context context) {
		Selection sel = new selectedNodesSelection ();
		SelectionTools.select(SelectionTools.innitializeSelection(sel, context), context);
		SelectionTools.deleteNodes(sel.getSelectedNodes(), context);
	}
	
	public static void selectByNType (Item item, Object info, Context context) {
		InputDialogNType inputPanel = new InputDialogNType();
		if (!inputPanel.getInput()) {
			System.err.println("Input Panel for NType has failed");
			return;
		}
		String ntype = inputPanel.getNType();
		Selection sel = new NTypeSelection(ntype);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, context), context);
		
//		System.err.println("--- selected by NType ---");
//		for(Node node : sel.getSelectedNodes()) {
//			System.err.println("NodeId: "+node.getId()+" NType: "+node.getNType());
//		}
		
		context.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_NType_done"));
	}
	
	public static void selectByParentNType (Item item, Object info, Context context) {
		
		InputDialogNType inputPanel = new InputDialogNType();
		
		if (!inputPanel.getInput()) {
			System.err.println("Input Panel for ParentNType has failed");
			return;
		}
		String ntype = inputPanel.getNType();
		Selection sel = new ParentNTypeSelection(ntype);
		SelectionTools.select(SelectionTools.innitializeSelection(sel, context), context);
	
//		System.err.println("--- selected by NType ---");
//		for(Node node : sel.getSelectedNodes()) {
//			System.err.println("NodeId: "+node.getId()+" NType: "+node.getNType());
//		}
		
		context.getWorkbench().logGUIInfo(Selection.I18N.msg("selection_ParentNType_done"));
	}
}
