package de.grogra.imp3d.selection.objects;

public class Interval {
	
	double start;
	double end;
	int axisMask;
	
	public final static int X_AXIS = 0x0001;
	public final static int Y_AXIS = 0x0002;
	public final static int Z_AXIS = 0x0004;
	
	public Interval(double start, double end, int axis) {
		this.start = start;
		this.end = end;
		this.axisMask = axis;
	}
	
	public Interval() {
		axisMask = -1;
		start = Integer.MIN_VALUE;
		end = Integer.MAX_VALUE;
	}

	public double getStart() {
		return start;
	}
	public double getEnd() {
		return end;
	}
	public int getAxisMask() {
		return axisMask;
	}

	public void setStart (double startAngle) {
		this.start = startAngle;
	}
	
	public void setEnd (double endAngle) {
		this.end = endAngle;
	}

	public void setAxisMask(int axisMask) {
		this.axisMask = axisMask;
	}
}