package de.grogra.imp3d.selection.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.vecmath.Point3d;

import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Node;
import de.grogra.rgg.Library;

public class SceneBoundaries extends VisitorImpl{
	
	List<Point3d> nodePositions = new ArrayList<>();
	List<Node> nodes = new ArrayList<>();
	double epsilon = 1.0E-12;
	
	
	protected double minX, maxX, minY, maxY, minZ, maxZ;
	
	public SceneBoundaries () {
		this.minX = this.minY = this.minZ = Integer.MAX_VALUE;
		this.maxX = this.maxY = this.maxZ = Integer.MIN_VALUE;
	}
	
	@Override
    public Object visitEnter (Path path, boolean isNode)
    {
		if (!isNode) {
			return null;
		}
        Node node= (Node) path.getObject(-1);
    	Point3d point = Library.location(node);
    	nodes.add(node);
    	nodePositions.add(point);
    	
    	if(point.x < minX) {
    		minX = point.x - epsilon;
    	}
    	if(point.x > maxX) {
    		maxX = point.x + epsilon;
    	}
    	if(point.y < minY) {
    		minY = point.y - epsilon;
    	}
    	if(point.y > maxY) {
    		maxY = point.y + epsilon;
    	}
    	if(point.z < minZ) {
    		minZ = point.z - epsilon;
    	}
    	if(point.z > maxZ) {
    		maxZ = point.z + epsilon;
        }
        return null;
    }
    @Override
    public Object visitInstanceEnter ()
    {
        return STOP;
    }
	
	public double getMinX () {
		return minX;
	}
	
	public double getMaxX() {
		return maxX;
	}
	
	public double getMinY() {
		return minY;
	}
	
	public double getMaxY() {
		return maxY;
	}
	
	public double getMinZ() {
		return minZ;
	}
	
	public double getMaxZ() {
		return maxZ;
	}
	
	public void printOutNodePositions () {
		for(int i = 0; i < nodes.size(); i ++) {
			System.err.println("NodeID: " + nodes.get(i).getId() +
					" - Position: " +nodePositions.get(i).x +"/"+ nodePositions.get(i).y +"/"+ nodePositions.get(i).z);
		}
	}
	
	public void printOutBoudnaries () {
		System.err.println("X:[" + minX +","+ maxX + "]");
		System.err.println("Y:[" + minY +","+ maxY + "]");
		System.err.println("Z:[" + minZ +","+ maxZ + "]");
	}
}