package de.grogra.imp3d.selection.impl;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;
import de.grogra.rgg.Library;

public class selectedNodesSelection extends Selection{

	@Override
	protected boolean acceptImpl(Node node) {
		return Library.isSelected(node);
	}

}
