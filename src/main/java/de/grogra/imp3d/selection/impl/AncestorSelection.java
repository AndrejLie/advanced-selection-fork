package de.grogra.imp3d.selection.impl;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;
import de.grogra.rgg.Library;
//change name to childrenSelection TODO
public class AncestorSelection extends Selection {
		
	public AncestorSelection(Object obj) {
		if (!(obj instanceof Node)) {
			return;
		}
		this.obj=obj;
	}

	@Override
	protected boolean acceptImpl(Node node) {
		return Library.isAncestor((Node)obj, node, Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE);
	}
}
