package de.grogra.imp3d.selection.impl;

import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.imp3d.selection.Selection;

/**
 * Select nodes based on the NType of their parent
 */
public class ParentNTypeSelection extends Selection{

	public ParentNTypeSelection(Object obj) {
		if (!(obj instanceof String)) {
			return;
		}
		this.obj=obj;
	}
	
	@Override
	protected boolean acceptImpl(Node node) {
		for (Edge e = node.getFirstEdge (); e != null; e = e.getNext (node))
		{
			if (e.getTarget () == node)
			{
				if ((e.getSource ()).getNType().getName().equals((String)obj)) {
					return true;
				}
			}
		}
		return false;
	}

}
