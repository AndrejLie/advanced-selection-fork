package de.grogra.imp3d.selection.impl;

import java.awt.datatransfer.SystemFlavorMap;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.rgg.Library;

public class AxisIntervalSelection extends Selection {
	
	public AxisIntervalSelection(Object obj) {
		if (!(obj instanceof Interval)) {
			return;
		}
		this.obj=obj;
	}

	@Override
	protected boolean acceptImpl(Node node) {
		return isInInterval(node, (Interval)obj);
	}
	
	public boolean isInInterval (Node node, Interval interval) {
		Point3d point = Library.location(node);	
		if(interval.getAxisMask() == 1) {
			if(interval.getStart() <= point.x && point.x < interval.getEnd()) {
				printNode(node, point);
				return true;
			}
		}
		if(interval.getAxisMask() == 2) {
			if(interval.getStart() <= point.y && point.y < interval.getEnd()) {
				printNode(node, point);
				return true;
			}
		}
		if(interval.getAxisMask() == 4) {
			if(interval.getStart() <= point.z && point.z < interval.getEnd()) {
				printNode(node, point);
				return true;
			}
		}
		System.err.println("--- NodeId: "+node.getId()+" Position: ["+point.x+", "+point.y+", "+point.z+"]");
		return false;
	}
	
	public void printNode(Node node, Point3d point) {
		System.err.println("Node: "+node.getId()+" X:"+point.x+" Y:" +point.y+" Z:"+point.z);
	}
}
