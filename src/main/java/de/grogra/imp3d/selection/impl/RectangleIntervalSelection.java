package de.grogra.imp3d.selection.impl;

import javax.vecmath.Point3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.rgg.Library;
import de.grogra.imp3d.selection.impl.AxisIntervalSelection;

public class RectangleIntervalSelection extends Selection{
	
	protected Object objX, objY, objZ;

	public RectangleIntervalSelection(Object objX, Object objY, Object objZ) {
		if (!((objX instanceof Interval) && (objY instanceof Interval) && (objZ instanceof Interval))) {
			return;
		}
		this.objX=objX;
		this.objY=objY;
		this.objZ=objZ;
	}

	@Override
	protected boolean acceptImpl(Node node) {
		return isInRectangle(node, (Interval)objX, (Interval)objY, (Interval)objZ); 
	}
	
	public boolean isInRectangle(Node node, Interval x, Interval y, Interval z) {
		Point3d point = Library.location(node);	
		if(x.getStart() <= point.x && point.x < x.getEnd()) {
			if(y.getStart() <= point.y && point.y < y.getEnd()) {
				if(z.getStart() <= point.z && point.z < z.getEnd()) {
					return true;
				}
			}
		}
		return false;
	}
}
