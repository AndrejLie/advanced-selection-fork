package de.grogra.imp3d.selection.impl;

import javax.vecmath.Point3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.objects.*;
import de.grogra.rgg.Library;

public class AngleSelection extends Selection {
	
	// epsilon of 1e^-10 varianz
	private boolean includeNodeOnZero;
	
	public AngleSelection(Object obj, boolean includeNodeOnZero) {
		if (!(obj instanceof Interval)) {
			return;
		}
		this.obj=obj;
		this.includeNodeOnZero = includeNodeOnZero;
	}

	@Override
	protected boolean acceptImpl(Node node) {
		return isInAngle(node, (Interval)obj, includeNodeOnZero);
	}	
	
	/**
	 * this method checks for each node if it is the type of a Null object which have the attribute 
	 * for the translation on the x, y and z axes and initiates the actual method for the selection.
	 *  
	 * @param edges the list of all objects with the type of Edge
	 * @param startAngle the opening degree
	 * @param endAngle the closing degree
	 * @param includeNodeOnZero a boolean if nodes of the distance 0 shall be included into the selection
	 * @return
	 */
	public boolean isInAngle(Node edge, Interval myAngle, boolean includeNodeOnZero)
	{
		if (!(edge instanceof Node)) {
			return false;
		}
		Point3d point = Library.location((Node)edge);
		double x = point.x;
		double y = point.y;
		double z = point.z;
		double start = myAngle.getStart();
		double end = myAngle.getEnd(); 
		boolean inOrigin = false;
		double angle = de.grogra.imp3d.selection.Utils.radToDeg(Math.atan2(point.y, point.x));
			
		//1) TODO check if points are matched with the right axis
		//2) TODO if clauses seem too complex
		int axisMask = myAngle.getAxisMask();
		if (axisMask == Interval.X_AXIS) {
			inOrigin = de.grogra.imp3d.selection.Utils.polarRadius(y, z) == 0;
			if (includeNodeOnZero && inOrigin) {
				return true;
			} else if(!includeNodeOnZero && inOrigin) {
				return false;
			}
		}
		else if (axisMask == Interval.Y_AXIS) {
			inOrigin = de.grogra.imp3d.selection.Utils.polarRadius(x, z) == 0;
			if(includeNodeOnZero && inOrigin) {
				return true;
			} else if(!includeNodeOnZero && inOrigin) {
				return false;
			}
		}
		else if (axisMask == Interval.Z_AXIS){
			inOrigin = de.grogra.imp3d.selection.Utils.polarRadius(x, y) == 0;
			if (includeNodeOnZero && inOrigin) {
				return true;
			} else if(!includeNodeOnZero && inOrigin) {
				return false;
			}
		}
		if(start == 0.0 && end == 0.0 && !inOrigin) {
			return false;
		}
		if (angle < 0.0) { 
			angle += 360.0; 
		}
		if ((start <= angle)  && (angle  <= end)) {
			//System.err.println("NodeID: "+edge.getId()+" Angle: "+angle);
			return true; 
		}
//		System.err.println("--- nodes that are not in the interval ---");
//		System.err.println("NodeID: "+edge.getId()+" Angle: "+angle);
		return false;
	}
}
