package de.grogra.imp3d.selection.impl;

import javax.vecmath.Point3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.selection.Selection;
import de.grogra.rgg.Library;
import de.grogra.vecmath.geom.Volume;

public class VolumeSelection extends Selection{

	public VolumeSelection(Object obj) {
		if (!(obj instanceof Null)) {
			return;
		}
		this.obj = obj;
	}
	
	@Override
	protected boolean acceptImpl(Node node) {
		return contains(node);
	}

	
	/**
	 * this method invokes a vecmath.geom object corresponding to the primitive imp3d object with volume
	 * to get access to the implemented contains-method that checks if a node is in the volume
	 * 
	 * @param node node of the scene tree
	 * @return boolean if an Edge object is in a volume of a vecmath.geometry object that is 
	 * corresponding to the imp3d object
	 */
	public boolean contains(Node node) {
		if (!(obj instanceof Node)) {
			return false;
		}
		Volume body = Library.volume ((Node) obj);
		Point3d point = Library.location(node);
		return body.contains(point, true);
	}
	
//	/**
//	 * every node of the graph is given to a method that checks if it is in a volume
//	 * 
//	 * @param <T> IMP3D Object to select upon
//	 * @param selector object you do upon the selection method based on the given object 
//	 * @param edges list of all edges
//	 * @return list of selected edges
//	 */
//	private static <T> void filterEdgesVolume(T selector, List<Edge> edges, Context ctx)
//	{
//		List<Edge> filteredList = new ArrayList<>();
//		
//		for(int i = 0; i < edges.size(); i++) {
//			if(Selection.containsEdge(selector, edges.get(i))) {
//				filteredList.add(edges.get(i));
//			}
//		}
//		filteredList.remove(selector);
//		Selection.select(filteredList, ctx);
//	}

	
}
