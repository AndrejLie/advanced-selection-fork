package de.grogra.imp3d.selection.impl;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;

public class AllNodesSelection extends Selection{

	@Override
	protected boolean acceptImpl(Node node) {
		return node instanceof Node;
	}
	
}
