package de.grogra.imp3d.selection.impl;

import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.imp3d.selection.Selection;

/**
 * Select nodes based on their NType
 */
public class NTypeSelection extends Selection{

	public NTypeSelection(Object obj) {
		if (!(obj instanceof String)) {
			return;
		}
		this.obj=obj;
	}
	
	@Override
	protected boolean acceptImpl(Node node) {
		NType ntype = node.getNType();
		return ntype.getName().equals((String)obj);
	}

}
