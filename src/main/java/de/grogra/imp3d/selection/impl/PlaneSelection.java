package de.grogra.imp3d.selection.impl;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Plane;
import de.grogra.imp3d.selection.Selection;
import de.grogra.rgg.Library;

public class PlaneSelection extends Selection {

	private boolean over;
	
	public PlaneSelection(Object obj, boolean isOver) {
		if (!(obj instanceof Plane)) {
			return;
		}
		this.obj=obj;
		this.over=isOver;
	}

	@Override
	protected boolean acceptImpl(Node node) {
		return isEdgeOverOrUnderPlane((Plane)obj, node, over);
	}
	
	/**
	 * This function checks if a node is above or below any given plane by using the
	 * normal vector derived from the matrix of the plane and therefore comparing the
	 * node position with the normal vector.
	 * 
	 * @param edge Graph node
	 * @param plane primitive object: plane
	 * @return a boolean that indicates if the node has to be selected in the following process
	 */
	public boolean isEdgeOverOrUnderPlane (Plane plane, Node edge, boolean selectOver) 
	{	
		if(!(edge instanceof Node))	{
			return false;
		}
		//Ebene als Matrix dargestellt
		Matrix4d matrix = plane.getLocalTransformation();
		//Ebene Koordinate
		Vector3d position = plane.getTranslation();
		//Normalvektor der Ebene
		Vector3d normal = new Vector3d(matrix.m02, matrix.m12, matrix.m22);
		//Knoten Koordinate
		Point3d point = Library.location((Node) edge);
		
		//differenzVektor
		position.sub(point);
		//wenn Skalarprodukt > 0 ist, dann liegt Punkt überhalb der Ebene  
		return selectOver ? position.dot(normal) < 0 : position.dot(normal) > 0;
	}
	
}
