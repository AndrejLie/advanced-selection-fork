package de.grogra.imp3d.selection;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.grogra.imp3d.selection.dialogs.DialogTool;
import de.grogra.imp3d.selection.objects.Interval;

public class InputDialogAxisIntervalSelection {

	
		double start, end;
		String[] choicesAxis = {"X-Axis", "Y-Axis", "Z-Axis"}; 
		int axisMask;

		JPanel panel = new JPanel ();
		JTextField startIntervalField = new JTextField(5);
		JTextField endIntervalField = new JTextField(5);
		JCheckBox checkBox = new JCheckBox(DialogTool.I18N.getString("degreeSelectionCheckbox"));
		JComboBox<String> axisSelection = new JComboBox<String>(choicesAxis);
		
		public InputDialogAxisIntervalSelection() {
			
			this.end = Double.MIN_VALUE;  
			this.start = Double.MAX_VALUE;
			this.axisMask = -1;
			
			panel.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(0,5,5,20);
			
			gbc.gridx=0;
			gbc.gridy=0;
			panel.add(new JLabel("start interval:"), gbc);
			
			gbc.gridx=1;
			gbc.gridy=0;
			panel.add(startIntervalField, gbc);
			
			gbc.gridx=2;
			gbc.gridy=0;
			panel.add(new JLabel("end interval:"), gbc);
			
			gbc.gridx=3;
			gbc.gridy=0;
			panel.add(endIntervalField, gbc);
			
			gbc.gridx=0;
			gbc.gridy=1;
			panel.add(axisSelection);
			
		}
		
		//store user's input into variables
		public boolean getInput() {
			
			boolean success = DialogTool.showInputDialog("inputTitle", panel);
			if(!success) {
				return success;
			}
			
			try {
					start = Double.parseDouble(startIntervalField.getText());
					end = Double.parseDouble(endIntervalField.getText());
					if ( start >= end ) {
						return DialogTool.handleException("errorIntervalBoundaries", DialogTool.I18N.getString("labelErrorIntervalBoundaries"));
					}
					
			} catch (NumberFormatException nfe) {
					return DialogTool.handleException("errorNumber",DialogTool.I18N.getString("labelErrorNumber"));
			}
			if(axisSelection.getSelectedItem() == null) {
				return DialogTool.handleException("errorComboBoxNullItem", DialogTool.I18N.getString("labelErrorComboBoxNullItem"));
			}
			String axisString = (String) axisSelection.getSelectedItem();
			if (axisString.equals("X-Axis")) {
				axisMask = Interval.X_AXIS;
			} else if (axisString.equals("Y-Axis")) {
				axisMask = Interval.Y_AXIS; 
			} else if (axisString.equals("Z-Axis")) {
				axisMask = Interval.Z_AXIS;
			} else {
				System.err.println(axisString);
				return DialogTool.handleException("errorAxisSelection", DialogTool.I18N.getString("labelErrorAxisSelection"));
			}
			//return DialogTool.handleException("errorAxis", DialogTool.I18N.getString("labelErrorAxis"));
			return success;
		}

		public double getStart() {
			return this.start;
		}
		
		public double getEnd() {
			return this.end;
		}
		
		public int getAxisMask() {
			return this.axisMask;
		}
}
