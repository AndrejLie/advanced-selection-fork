package de.grogra.imp3d.selection;

import de.grogra.util.I18NBundle;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.dialogs.DialogTool;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class for selection object. A selection is an object that can be accepted against
 * a node. A selection can be a group of other selection. 
 * i.e. two Selections: 1) is Node in a Box, and 2) is over Plane - the Selection group
 * accept the Node n, iff Selection 1 and Selection 2 accept n (e.g. n is in the Box and over Plane).
 */
public abstract class Selection extends VisitorImpl{
	 
	//public static final I18NBundle I18N = I18NBundle.getInstance(Selection.class);
	public static final I18NBundle I18N = I18NBundle.getInstance(Selection.class);
	/*
	 * A selection can be a group of selection. In that case all of its sub selection must
	 * be true for the global selection to be true.
	 */
	private	 List<Selection> selectionList = new ArrayList<>();
	/*
	 * The object against what the selection is tested.
	 */
	protected  Object obj;
	/*
	 * This list is populated with the selected node when the selection is preformed
	 */
	private	 List<Node> selectedNodes = new ArrayList<>();
		
	//accepts ONLY if it is in EVERY selection
	
	//addition variable : type of selection I want OR/XOR/...
	//additional protected parameter in selection
	//each selection needs to be aware of its condition
	public boolean accept (Node node) {
		if (selectionList.size() > 1) {
			for (int i = 0; i < selectionList.size(); i++) {
				if (!selectionList.get(i).accept(node)) {
					return false;
				}
			}
			return true;
		}else {
			return acceptImpl(node);
		}
	}
	
	protected abstract boolean acceptImpl(Node node);
	
	public Object getSelectionObject() {
		return obj;
	}

	public void addSelection(Selection s) {
		if (selectionList.contains(s)) {
			return;
		}
		selectionList.add(s);
	}
	
	public List<Node> getSelectedNodes(){
		return selectedNodes;
	}
	
	@Override
    public Object visitEnter (Path path, boolean isNode)
    {
		if (!isNode) {
			return null;
		}
        Node node= (Node) path.getObject(-1);
        if (accept(node)) {
        	selectedNodes.add(node);
        }
        return null;
    }
    @Override
    public Object visitInstanceEnter ()
    {
        return STOP;
    }
}
