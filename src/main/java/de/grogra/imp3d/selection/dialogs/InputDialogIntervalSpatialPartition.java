package de.grogra.imp3d.selection.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.util.I18NBundle;

public class InputDialogIntervalSpatialPartition {
	
	//I18NBundle is used for translation purposes (see Ressources.properties)
		public static final I18NBundle I18N = I18NBundle.getInstance(de.grogra.imp3d.selection.dialogs.DialogTool.class);
		//numbers of sectors
		static double size;
		//shift of the axis
		static double translation;
		//on which axis should be selected
		String[] choicesAxis = {"X-Axis", "Y-Axis", "Z-Axis"}; 
		int axisMask;
		
		//JCheckBox checkBox = new JCheckBox(DialogTool.I18N.getString("degreeSelectionCheckbox"));
		JComboBox axisSelection = new JComboBox(choicesAxis);
		
		//panel core
		JLabel label;
		JTextField sizeField;
		JTextField translationField;
		JPanel panel;
		
		//ButtonGroup group;
		String title;

		public InputDialogIntervalSpatialPartition () {
			
			this.size = -1;
			this.translation = 0;
			this.axisMask = -1;
			
			this.label = new JLabel (I18N.getString("planeSpatialLabel"));
			this.sizeField = new JTextField(5);
			this.translationField = new JTextField(5);
			this.panel = new JPanel ();
			
			this.title = I18N.getString("planeSpatialTitle");
			
			//---//
			
			this.panel.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(0,5,5,20);

			gbc.gridwidth=2;
			gbc.gridx=0;
			gbc.gridy=0;
			this.panel.add(this.label, gbc);
			
			
			gbc.gridwidth=3;
			gbc.gridx=0;
			gbc.gridy=1;
			this.panel.add(new JLabel(" "), gbc);
			
			gbc.gridwidth=2;
			gbc.gridx=2;
			gbc.gridy=0;
			panel.add(axisSelection);
			
			gbc.gridwidth=2;
			gbc.gridx=0;
			gbc.gridy=2;
			this.panel.add(new JLabel(I18N.getString("planeSpatialSize")), gbc);
			
			gbc.gridwidth=1;
			gbc.gridx=2;
			gbc.gridy=2;
			this.panel.add(this.sizeField, gbc);
			
			gbc.gridwidth=2;
			gbc.gridx=0;
			gbc.gridy=3;
			this.panel.add(new JLabel(I18N.getString("planeSpatialTranslation")), gbc);
			
			gbc.gridwidth=1;
			gbc.gridx=2;
			gbc.gridy=3;
			this.panel.add(this.translationField, gbc);
	
			
		
		}
		
		//store user's input into variables
		public boolean getInput() {
			
			boolean success = DialogTool.showInputDialog("inputTitle", panel);
			if(!success) {
				return success;
			}
			try {	
				this.size = Double.parseDouble(sizeField.getText());
				if(this.size <= 0) {
					DialogTool.handleException("errorInvalidNumberValue", "labelInvalidNumberValue");
				}
				this.translation = Double.parseDouble(translationField.getText());
			}
			catch (NumberFormatException nfe) {
				return DialogTool.handleException("errorDataType", "labelErrorDataType");
			}
			String axisString = (String) axisSelection.getSelectedItem();
			if (axisString.equals("X-Axis")) {
				axisMask = Interval.X_AXIS;
			} else if (axisString.equals("Y-Axis")) {
				axisMask = Interval.Y_AXIS; 
			} else if (axisString.equals("Z-Axis")) {
				axisMask = Interval.Z_AXIS;
			} else {
				System.err.println(axisString);
				return DialogTool.handleException("errorAxisSelection", DialogTool.I18N.getString("labelErrorAxisSelection"));
			}
			return success;	
		}
		
		public double getRange() {
			return this.size;
		}
		
		public double getTranslation() {
			return this.translation;
		}
		
		public int getAxisMask() {
			return this.axisMask;
		}
}
