package de.grogra.imp3d.selection.dialogs;

import javax.swing.JLabel;
import javax.swing.JPanel;

import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.I18NBundle;

public class DialogTool {

	//I18NBundle is used for translation purposes (see Ressources.properties)
	public static final I18NBundle I18N = I18NBundle.getInstance(DialogTool.class);
	
	private static int showDialog(String titleKey, JPanel panel, int windowType) {
		
		return Workbench.current().getWindow().showDialog(
				I18N.getString(titleKey), 
				panel,
				windowType);

	}
	
	protected static void showMessageDialog(String titleKey, JPanel panel) {
		
		DialogTool.showDialog(titleKey, panel, Window.RESIZABLE_PLAIN_MESSAGE);
	}
	
	
	public static boolean showInputDialog(String titleKey, JPanel panel) {
		
		int result = DialogTool.showDialog(titleKey, panel, Window.RESIZABLE_OK_CANCEL_MESSAGE);
		return result == Window.YES_OK_RESULT;
	}
	
	//error management
	//if it doesnt work, the string has to be put in the Resources.property file in
	//src/main/resources
	public static boolean handleException (String output, String label) {
		
		JPanel error = new JPanel();
		JLabel errorLabel = new JLabel(I18N.getString(label));
		error.add(errorLabel);
		
		Workbench.current().getWindow().showDialog(
			I18N.getString(output), 
			error,
			Window.RESIZABLE_PLAIN_MESSAGE);
		
		return false;
	}
	
}
