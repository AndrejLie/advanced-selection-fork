package de.grogra.imp3d.selection.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.util.I18NBundle;


public class InputDialogDegreeSpatialPartition {

	//I18NBundle is used for translation purposes (see Ressources.properties)
	public static final I18NBundle I18N = I18NBundle.getInstance(de.grogra.imp3d.selection.dialogs.DialogTool.class);
	//numbers of sectors
	static int nSectors;
	//on which axis should be selected
	String[] choicesAxis = {"X-Axis", "Y-Axis", "Z-Axis"}; 
	public int axisMask;
	
	//JCheckBox checkBox = new JCheckBox(DialogTool.I18N.getString("degreeSelectionCheckbox"));
	JComboBox axisSelection = new JComboBox(choicesAxis);
	
	//panel core
	JLabel label;
	JTextField nSectorField;
	JPanel panel;
	
	//ButtonGroup group;
	String title;
	
	//fill panel
	public InputDialogDegreeSpatialPartition() {	
			
		this.nSectors = -1;
		
		this.label = new JLabel (I18N.getString("degreeSpatialPartitionLabel"));
		this.nSectorField = new JTextField(5);
		this.panel = new JPanel ();
		
		this.title = I18N.getString("degreeSpatialPartitionTitle");
		
		//---//
		
		this.panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0,5,5,20);

		gbc.gridwidth=2;
		gbc.gridx=0;
		gbc.gridy=0;
		this.panel.add(this.label, gbc);
		
		gbc.gridwidth=4;
		gbc.gridx=0;
		gbc.gridy=1;
		this.panel.add(new JLabel(" "), gbc);
		
		gbc.gridwidth=2;
		gbc.gridx=0;
		gbc.gridy=2;
		this.panel.add(new JLabel(I18N.getString("degreeSpatialPartitionCombobox")), gbc);
		
		gbc.gridwidth=1;
		gbc.gridx=2;
		gbc.gridy=2;
		this.panel.add(this.nSectorField, gbc);
		
		gbc.gridwidth=2;
		gbc.gridx=2;
		gbc.gridy=0;
		panel.add(axisSelection);
	}	
		
	//store user's input into variables
	public boolean getInput() {
		
		boolean success = DialogTool.showInputDialog("inputTitle", panel);
		if(!success) {
			return success;
		}
		try {	
				this.nSectors = Integer.parseInt(this.nSectorField .getText());
				if(this.nSectors <= 0) {
					DialogTool.handleException("errorInvalidNumberValue", "labelInvalidNumberValue");
				}	
		}
		catch (NumberFormatException nfe) {
			return DialogTool.handleException("errorDataType", "labelErrorDataType");
		}
		if(axisSelection.getSelectedItem() == null) {
			return DialogTool.handleException("errorComboBoxNullItem", DialogTool.I18N.getString("labelErrorComboBoxNullItem"));
		}
		String axisString = (String) axisSelection.getSelectedItem();
		if (axisString.equals("X-Axis")) {
			axisMask = Interval.X_AXIS;
		} else if (axisString.equals("Y-Axis")) {
			axisMask = Interval.Y_AXIS; 
		} else if (axisString.equals("Z-Axis")) {
			axisMask = Interval.Z_AXIS;
		} else {
			System.err.println(axisString);
			return DialogTool.handleException("errorAxisSelection", DialogTool.I18N.getString("labelErrorAxisSelection"));
		}
		return success;	
	}
	
	public int getAxisMask () {
		return axisMask;
	}
	
	public int getNSectors() {
		return this.nSectors;
	}
}
