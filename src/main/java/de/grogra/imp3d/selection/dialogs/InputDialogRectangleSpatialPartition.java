package de.grogra.imp3d.selection.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.grogra.imp3d.selection.objects.Interval;
import de.grogra.util.I18NBundle;


/**
 * a gui that pops up after the polarSelect button is pressed to request parameters for the selection
 * @return an array of objects that contains three elements: 
 * a span of degree with start and end as a boolean to include nodes with a distance of zero
 */

public class InputDialogRectangleSpatialPartition {
	
	//I18NBundle is used for translation purposes (see Ressources.properties)
	public static final I18NBundle I18N = I18NBundle.getInstance(de.grogra.imp3d.selection.dialogs.DialogTool.class);
	double size;
	//panel core
	JPanel panel = new JPanel ();
	JTextField sizeField = new JTextField(5);
	//fill panel
	public InputDialogRectangleSpatialPartition () {
		
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0,5,5,20);
		
		gbc.gridx=0;
		gbc.gridy=0;
		panel.add(new JLabel(I18N.getString("rectangleSpatialSize")), gbc);
		
		gbc.gridx=1;
		gbc.gridy=0;
		panel.add(sizeField, gbc);
	}	
	
	//store user's input into variables
	public boolean getInput() {
		
		boolean success = DialogTool.showInputDialog("inputTitle", panel);
		if(!success) {
			return success;
		}
		try {
				this.size = Double.parseDouble(sizeField.getText());
				if ( this.size <= 0 ) {
					return DialogTool.handleException("errorNoPositiveNumber", "labelErrorNoPositiveNumber");
				}
		} catch (NumberFormatException nfe) {
				//return DialogTool.handleException("errorNumber",DialogTool.I18N.getString("labelErrorNumber"));
				return DialogTool.handleException("errorDataType", "labelErrorDataType");
		}
		//return DialogTool.handleException("errorAxis", DialogTool.I18N.getString("labelErrorAxis"));
		return success;
	}
		
	public double getSize () {
		return this.size;
	}
}
