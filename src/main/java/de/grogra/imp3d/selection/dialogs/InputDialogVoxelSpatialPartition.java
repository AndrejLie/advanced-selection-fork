package de.grogra.imp3d.selection.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import de.grogra.imp3d.selection.Selection;
import de.grogra.util.I18NBundle;

public class InputDialogVoxelSpatialPartition {
	
	
	//I18NBundle is used for translation purposes (see Ressources.properties)
		public static final I18NBundle I18N = I18NBundle.getInstance(de.grogra.imp3d.selection.dialogs.DialogTool.class);
		
		static double distance;
		

		//on which axis should be selected
		
		//panel core
		JLabel label;
		JTextField distanceField;
		JPanel panel;
		
		ButtonGroup group;
		String title;

		public InputDialogVoxelSpatialPartition () {
			
			this.distance = -1;
		
			this.label = new JLabel (I18N.getString("voxelSectorLabel"));
			this.distanceField = new JTextField(5);
			this.panel = new JPanel ();
	
			this.group = new ButtonGroup();
			this.title = I18N.getString("voxelSectorTitle");
			
			//---//
			
			this.panel.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(0,5,5,20);

			gbc.gridwidth=3;
			gbc.gridx=0;
			gbc.gridy=0;
			this.panel.add(this.label, gbc);
			
			gbc.gridwidth=3;
			gbc.gridx=0;
			gbc.gridy=1;
			this.panel.add(new JLabel(" "), gbc);
			
			//TODO change this shit so it is not combobox anymore and label the textfield
			gbc.gridwidth=2;
			gbc.gridx=0;
			gbc.gridy=2;
			this.panel.add(new JLabel(I18N.getString("voxelSectorDistance")), gbc);
			
			gbc.gridwidth=1;
			gbc.gridx=2;
			gbc.gridy=2;
			this.panel.add(this.distanceField, gbc);
		}
		
		//store user's input into variables
		public boolean getInput() {
			
			boolean success = DialogTool.showInputDialog("inputTitle", panel);
			if(!success) {
				return success;
			}
			try {	
					this.distance = Double.parseDouble(distanceField.getText());
					if(this.distance <= 0) {
						DialogTool.handleException("errorInvalidNumberValue", "labelInvalidNumberValue");
					}
			}
			catch (NumberFormatException nfe) {
				return DialogTool.handleException("errorDataType", "labelErrorDataType");
			}
			return success;	
		}
		
		public double getDistance() {
			return this.distance;
		}
}