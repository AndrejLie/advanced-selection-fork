package de.grogra.imp3d.selection.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.grogra.graph.impl.Node.NType;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.TypeItem;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.I18NBundle;

public class InputDialogNType {
	
	
	//I18NBundle is used for translation purposes (see Ressources.properties)
		public static final I18NBundle I18N = I18NBundle.getInstance(de.grogra.imp3d.selection.dialogs.DialogTool.class);
		String ntype;
		//panel core
		JPanel panel = new JPanel ();
		JTextField ntypeField = new JTextField(5);
		//fill panel
		public InputDialogNType () {
			panel.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(0,5,5,20);
			
			gbc.gridx=0;
			gbc.gridy=0;
			panel.add(new JLabel(I18N.getString("rectangleSpatialSize")), gbc);
			
			gbc.gridx=1;
			gbc.gridy=0;
			panel.add(ntypeField, gbc);
		}	
		
		//store user's input into variables
		public boolean getInput() {
			
			boolean success = DialogTool.showInputDialog("inputTitle", panel);
			if(!success) {
				return success;
			}
			try {
				ntype = ntypeField.getText();
				
				/*
				Registry r = Workbench.current().getRegistry();
				Item classes_directory = r.getItem("/classes");
		        Item[] classes = Item.findAll(classes_directory, ItemCriterion.INSTANCE_OF, TypeItem.class, true);
				for(Item type: classes) {
					if(ntypeField.getText().equals(type.getName())) {
						if(((TypeItem) type).getObject() instanceof NType) {
							this.ntype = (NType) ((TypeItem) type).getObject();
							return true;
						}
					}
				}*/
				
				
				/* ???
				if ( this.ntype  ) {
					return DialogTool.handleException("errorNoPositiveNumber", "labelErrorNoPositiveNumber");
				}
				*/
			} catch (NumberFormatException nfe) {
					//return DialogTool.handleException("errorNumber",DialogTool.I18N.getString("labelErrorNumber"));
					return DialogTool.handleException("errorDataType", "labelErrorDataType");
			}
			//return DialogTool.handleException("errorAxis", DialogTool.I18N.getString("labelErrorAxis"));
			return success;
		}
			
		public String getNType () {
			return this.ntype;
		}
	
}
