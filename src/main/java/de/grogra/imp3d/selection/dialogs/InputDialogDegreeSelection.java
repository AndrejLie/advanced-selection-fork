package de.grogra.imp3d.selection.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.grogra.imp3d.selection.objects.Interval;


/**
 * a gui that pops up after the polarSelect button is pressed to request parameters for the selection
 * @return an array of objects that contains three elements: 
 * a span of degree with start and end as a boolean to include nodes with a distance of zero
 */

public class InputDialogDegreeSelection {
	
	//I18NBundle is used for translation purposes (see Ressources.properties)
	
	//Angles for an interval 
	double startAngle, endAngle;
	//should elements without a distance to the referencing point be included?
	boolean zeroRadiusNodeIncluded = false;
	//axis options
	String[] choicesAxis = {"X-Axis", "Y-Axis", "Z-Axis"}; 
	
	//on which axis it should be selected
	int axisMask;
	//boolean xAxisSelected = false, zAxisSelected = false;
	
	//panel core
	JPanel panel = new JPanel ();
	JTextField startAngleField = new JTextField(5);
	JTextField endAngleField = new JTextField(5);
	JCheckBox checkBox = new JCheckBox(DialogTool.I18N.getString("degreeSelectionCheckbox"));
	//dropdown menu for axis selection
	JComboBox axisSelection = new JComboBox(choicesAxis);

	//fill panel
	public InputDialogDegreeSelection() {
		
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0,5,5,20);
		
		gbc.gridx=0;
		gbc.gridy=0;
		panel.add(new JLabel("start angle:"), gbc);
		
		gbc.gridx=1;
		gbc.gridy=0;
		panel.add(startAngleField, gbc);
		
		gbc.gridx=2;
		gbc.gridy=0;
		panel.add(new JLabel("end angle:"), gbc);
		
		gbc.gridx=3;
		gbc.gridy=0;
		panel.add(endAngleField, gbc);
		
		gbc.gridx=0;
		gbc.gridy=1;
		panel.add(axisSelection);
		
	}	
	
	//store user's input into variables
	public boolean getInput() {
		
		boolean success = DialogTool.showInputDialog("inputTitle", panel);
		if(!success) {
			return success;
		}
		
		try {
				startAngle = Double.parseDouble(startAngleField.getText());
				endAngle = Double.parseDouble(endAngleField.getText());
				if ( this.startAngle < 0 || this.endAngle < 0 ) {
					//DialogTool.handleException("errorInvalidNumberValue", "labelInvalidNumberValue");
					this.startAngle -= 360;
					this.endAngle -= 360;
				}
				if ( startAngle >= endAngle ) {
					return DialogTool.handleException("errorIntervalBoundaries", DialogTool.I18N.getString("labelErrorIntervalBoundaries"));
				}
				
		} catch (NumberFormatException nfe) {
				return DialogTool.handleException("errorNumber",DialogTool.I18N.getString("labelErrorNumber"));
		}
		if(axisSelection.getSelectedItem() == null) {
			return DialogTool.handleException("errorComboBoxNullItem", DialogTool.I18N.getString("labelErrorComboBoxNullItem"));
		}
		String axisString = (String) axisSelection.getSelectedItem();
		if (axisString.equals("X-Axis")) {
			axisMask = Interval.X_AXIS;
		} else if (axisString.equals("Y-Axis")) {
			axisMask = Interval.Y_AXIS; 
		} else if (axisString.equals("Z-Axis")) {
			axisMask = Interval.Z_AXIS;
		} else {
			System.err.println(axisString);
			return DialogTool.handleException("errorAxisSelection", DialogTool.I18N.getString("labelErrorAxisSelection"));
		}
		//return DialogTool.handleException("errorAxis", DialogTool.I18N.getString("labelErrorAxis"));
		return success;
	}
	
	//getters
	public double getStartAngle() {	
		return startAngle;
	}
	
	public double getEndAngle() {
		return endAngle;
	}
	
	public boolean isZeroRadiusNodeIncluded () {
		return zeroRadiusNodeIncluded;
	}
	public int getAxisMask () {
		return axisMask;
	}
}

