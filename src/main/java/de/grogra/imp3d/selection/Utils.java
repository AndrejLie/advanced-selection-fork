package de.grogra.imp3d.selection;

public class Utils {

	/**
	 * radiant to degree equation
	 * @param radian 
	 * @return degree
	 */
	public static double radToDeg (double radian)
	{
		return 180.0 * radian / Math.PI;
	}
	
	public static double polarRadius (double x, double y) {
		return Math.sqrt(x*x+y*y);
	}
}
