package de.grogra.imp3d.selection;

import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.edit.GraphSelection;

public class SelectionTools {

	public static Node getSelectedNode(Context c) {
		Object s = UIProperty.WORKBENCH_SELECTION.getValue (c.getWorkbench());
		if (!(s instanceof GraphSelection) || ((GraphSelection)s).size()<1) {
			return null;
		}
		Object n = ((GraphSelection)s).getObject(0);
		if (!(n instanceof Node)) {
			return null;
		}
		return (Node) n;
	}
	
	public static List<Node> getSelectedNodes(Context c) {
		Object s = UIProperty.WORKBENCH_SELECTION.getValue (c.getWorkbench());
		if (!(s instanceof GraphSelection) || ((GraphSelection)s).size()<1) {
			return null;
		}
		List<Node> l = new ArrayList<Node>();
		for (int i=0;i<((GraphSelection)s).size();i++) {
			Object n = ((GraphSelection)s).getObject(i);
			if (n instanceof Node) {
				l.add((Node)n);
			}
		}
		return (l.isEmpty())?null:l;
	}
	
	/*
	 * run the selection and select the node in the workbench
	 */
	public static List<Node> innitializeSelection(Selection sel, Context c) {
		Graph g = c.getWorkbench().getRegistry().getProjectGraph();
		Node root = (Node) g.getRoot(Graph.MAIN_GRAPH);
		sel.init (root.getCurrentGraphState (), EdgePatternImpl.TREE);
        g.accept (root, sel, null);
        return sel.getSelectedNodes();
	}
	
	/*
	 * run the selection and select the node in the workbench
	 */
	public static void select(List<Node> toSelect, Context c) {
        c.getWorkbench().select(toSelect.toArray(new Node[0]));
	}
	
	public static void deleteNodes(List<Node> nodesToDelete, Context context) {
		Node[] nodeArray = new Node[nodesToDelete.size()];
		nodesToDelete.toArray(nodeArray);
		//context.getWorkbench().select(nodeArray);
		//nodes has to be selected to be deleted
		context.getWorkbench().delete(nodeArray);
	}
}
