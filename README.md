# Advanced selection

The AdvancedSelection plugin provides 1) a set of tools for selecting nodes in the 3d view based on geometry/topology. 2) a navigation tool to ease the selection of nodes by hand in a complex graph.<br>
You can get started by creating selections of nodes using the menu in the 3d view: "selection>...".

You can then aggregate nodes based on several criteria "selection>aggregate>...".

All aggregations are listed in the explorer panel "panel>graph>aggregations".

**Note: this plugin is still in developement**.

